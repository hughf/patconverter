#!/bin/bash

WORKDIR=$(pwd)
TMPDIR=${WORKDIR}/tmp
CMDINST=$(command -v dos2unix)

# Make sure Dos2Unix cmd installed
if [[ ! $CMDINST ]]; then
	echo "Missing dos2unix command. Please ensure this is installed before running"
	exit 1
fi

# Check for args
if [[ $# -lt 1 ]]; then
	echo "Usage: patzip2tar.sh <patzip file or pattern>"
	echo "Example: patzip2tar.sh pat2024050601.zip  -- convert a single patch"
	echo "	       patzip2tar.sh pat*.zip -- convert a bunch of patches "
	echo "In both examples, the patch zip files are assumed to be in the same directory"
	exit 0
fi


# Use $@ in case we specify wild card, i.e. pat*.zip or *
for ZIPFILE in "$@"; do
	if [[ $ZIPFILE != *.zip || $ZIPFILE != pat* ]]; then
		echo "${ZIPFILE} not a windows patch file"
		continue
	fi 

	TARFILE=${ZIPFILE/%zip/tar}

	echo "Converting patch ${ZIPFILE} to ${TARFILE}"
	
	# (re)create the tmp file each time through the loop
	mkdir -p ${TMPDIR}
	# Move our zip file into temp
	mv ${ZIPFILE} ${TMPDIR}
	cd ${TMPDIR}
	
	# Unzip then remove the zip file
	unzip ${ZIPFILE}
	rm ${ZIPFILE}
	
	# Since this is a Windows patch, convert all contents to UNIX
	find . -name [a-z]*.[a-z]* -exec dos2unix --keepdate {} +
	
	# Tar up the contents we just unzipped and converted, then move tarball back 
	# to home directory
	tar cf ${TARFILE} .   
	mv ${TARFILE} ${WORKDIR}
	# Move back to our starting directory and delete the temp dir
	cd ${WORKDIR}
	rm -rf ${TMPDIR}
done





