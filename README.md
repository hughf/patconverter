# Patch Converter
## For OpenText patch files to convert from Windows to Linux
I wrote this bash script to convert one or more patches that are packed as zip file to be unix compatible tar ball files (.tar.gz).
The script takes a single argument, which is the name of the patch or a wildcard to specify many.

## Examples:
`patzip2tar.sh pat2024071901.zip` converts pat2024071901.zip to pat2024071701.tar
`patzip2tar.sh pat*.zip`  Converts all files matching pat*.zip to their equivalent patnnnnn.tar

